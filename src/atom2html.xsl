<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:atom="http://www.w3.org/2005/Atom">
	<xsl:output method="html" encoding="UTF-8"/>

	<xsl:template match="atom:feed">
		<html xml:lang="{@xml:lang}" lang="{@xml:lang}">
			<head>
				<meta charset="UTF-8"/>
				<meta name="theme-color" content="black"/>
				<meta name="viewport" content="width=device-width"/>
				<title>
					<xsl:copy-of select="atom:title/@*"/>
					<xsl:value-of select="atom:title"/>
				</title>
				<link rel="icon" href="{atom:icon}"/>
			</head>
			<body style="background: honeydew; margin: 0 .5em .5em">
				<header style="display: flex; align-items: baseline; flex-wrap: wrap">
					<h1>
						<small title="{atom:id}" aria-hidden="true">📂 </small>
						<a>
							<xsl:copy-of select="atom:link[not(@rel='self')]/@*"/>
							<code>
								<xsl:value-of select="atom:link[not(@rel='self')]/@href"/>
							</code>
						</a>
					</h1>
					<p xml:lang="en" lang="en" style="flex: 1 12ch; text-align: right">
						Last update on <xsl:apply-templates select="atom:updated"/>
					</p>
				</header>
				<div role="feed" style="display: grid; grid-gap: .5em; grid-template-columns: repeat(auto-fit, minmax(calc(16em + 5vw), 1fr))">
					<xsl:apply-templates select="atom:entry"/>
				</div>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="atom:entry">
		<xsl:variable name="type">
			<xsl:choose>
				<xsl:when test="atom:content/@type"><xsl:value-of select="atom:content/@type"/></xsl:when>
				<xsl:when test="atom:link/@type"><xsl:value-of select="atom:link/@type"/></xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="span">
			<xsl:choose>
				<xsl:when test="(starts-with($type, 'text/') or starts-with($type, 'application/pdf')) and position() &lt; 20">2</xsl:when>
				<xsl:otherwise>1</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<article id="{atom:id}" style="background: white; box-shadow: inset 0 -.5px 1.5px graytext; padding: .25em .5em .5em; grid-row-end: span {$span}; break-inside: avoid">
			<xsl:copy-of select="@*"/>
			<header style="display: flex; align-items: baseline; flex-wrap: wrap">
				<h2 style="flex: 1 auto; margin: .25em 0">
					<small title="{$type}">
						<xsl:choose>
							<xsl:when test="$type = 'xhtml' or $type = 'html' or $type = 'text'">📝 </xsl:when>
							<xsl:when test="starts-with($type, 'text/')">📄 </xsl:when>
							<xsl:when test="starts-with($type, 'font/')">🔣 </xsl:when>
							<xsl:when test="starts-with($type, 'image/')">🎨 </xsl:when>
							<xsl:when test="starts-with($type, 'audio/')">🎵 </xsl:when>
							<xsl:when test="starts-with($type, 'video/')">🎥 </xsl:when>
							<xsl:otherwise>🔗 </xsl:otherwise>
						</xsl:choose>
					</small>
					<a>
						<xsl:choose>
							<xsl:when test="atom:content/@src">
								<xsl:copy-of select="atom:content/@*[name() != 'src']"/>
								<xsl:attribute name="href">
									<xsl:value-of select="atom:content/@src"/>
								</xsl:attribute>
							</xsl:when>
							<xsl:when test="atom:link/@href">
								<xsl:copy-of select="atom:link/@*"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="href">
									<xsl:value-of select="concat('#', atom:id)"/>
								</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:apply-templates select="atom:title"/>
					</a>
				</h2>
				<xsl:text> </xsl:text>
				<span style="flex: 1 10ch; text-align: right">
					<xsl:apply-templates select="atom:updated"/>
				</span>
			</header>
			<div style="margin: .5em 0 1em">
				<xsl:apply-templates select="atom:summary"/>
			</div>
			<xsl:if test="atom:content">
				<xsl:choose>
					<xsl:when test="not(atom:content/@src)">
						<div>
							<xsl:apply-templates select="atom:content"/>
						</div>
					</xsl:when>
					<xsl:when test="$span = 2">
						<iframe title="" loading="lazy" style="width: 96%; height: 18em; resize: vertical">
							<xsl:copy-of select="atom:content/@*[name() != 'type']"/>
							<xsl:text> </xsl:text> <!-- “<iframe></iframe>”, not “<iframe/>” -->
						</iframe>
					</xsl:when>
					<xsl:when test="starts-with($type, 'image/') and position() &lt; 100">
						<a href="{atom:content/@src}">
							<img decoding="async" alt="" loading="lazy" style="width: 9em; height: 6em; object-fit: contain">
								<xsl:copy-of select="atom:content/@*[name() != 'type']"/>
							</img>
						</a>
					</xsl:when>
					<xsl:when test="starts-with($type, 'video/') and position() &lt; 150">
						<video controls="controls" style="width: 99%; height: auto; resize: vertical">
							<xsl:copy-of select="atom:content/@*[name() != 'type']"/> Can’t embed video ☹
						</video>
					</xsl:when>
					<xsl:when test="starts-with($type, 'audio/') and position() &lt; 150">
						<audio controls="controls">
							<xsl:copy-of select="atom:content/@*[name() != 'type']"/> Can’t embed audio ☹
						</audio>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
		</article>
	</xsl:template>

	<xsl:template match="atom:title | atom:summary | atom:content">
		<xsl:copy-of select="@*[name() != 'type']"/>
		<xsl:choose>
			<xsl:when test="@type = 'xhtml'">
				<xsl:copy-of select="*"/>
			</xsl:when>
			<xsl:when test="@type = 'html'">
				<xsl:value-of select="." disable-output-escaping="yes"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="atom:updated">
		<time datetime="{.}" title="{.}" style="white-space: nowrap">
			<xsl:value-of select="substring-before(., 'T')"/>
		</time>
	</xsl:template>
</xsl:stylesheet>
