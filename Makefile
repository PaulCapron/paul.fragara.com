KEYBASE_CONF = ~/.config/keybase/config.json
KEYBASE_PUBROOT := $(shell jq -r .mountdir <$(KEYBASE_CONF))/public
KEYBASE_PUBHOME := $(KEYBASE_PUBROOT)/$(shell jq -r .current_user <$(KEYBASE_CONF))

.PHONY: deploy

~/Public: src
	rsync -avh --delete $</ $@

$(KEYBASE_PUBHOME): ~/Public | $(KEYBASE_PUBROOT)
	rsync -rvh --delete $</ $@
$(KEYBASE_PUBROOT):
	run_keybase -a && sleep 2

deploy: $(KEYBASE_PUBHOME)
	netlify deploy --prod --dir=src
